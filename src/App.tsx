import React from 'react';
import logo from './logo.svg';
import './App.css';
import { ProjectListScreen } from './screens/project-list';
import { LoginScreen } from './screens/login';
import Index from './screens/demo/state';

function App() {
  return (
    <div className="App">
      <Index />
    </div>
  );
}

export default App;
