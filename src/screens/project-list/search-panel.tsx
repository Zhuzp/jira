import React from 'react';
import { useEffect, useState } from "react"

export interface User {
    id: string;
    name: string;
    email: string;
    title: string;
    organization: string;
}

interface SearchPanelProps {
    users: User[],
    param: {
        name: string;
        personId:string;
    },
    setParam: (param: SearchPanelProps['param']) => void;
}

export const SearchPanel = (props: SearchPanelProps) => {

    useEffect(() => {
        console.log(props)
    },[])
   
    return <form action="">
        <div>
            {/* // setParam(Object.assign({}, param, {name: evt.target.value})) */}
            <input type="text" value={props.param.name} onChange={evt => props.setParam({
                ...props.param,
                name: evt.target.value
            })} />
            <select value={props.param.personId} onChange={evt => props.setParam({
                ...props.param,
                personId: evt.target.value
            })}>
                <option value="">负责人</option>
                {
                    props.users.map(user => <option key={user.id} value={user.id}>{user.name}</option>)
                }
            </select>
        </div>
    </form>
}