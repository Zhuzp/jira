import React from 'react';
import { User } from './search-panel';

interface Project {
    id: string;
    name: string;
    personId: string;
    pin: boolean;
    organization: string;
}

interface ListProps {
    list: Project[];
    users: User[];
}

export const List = (props: ListProps) => {
    return (
        <table>
            <thead>
                <tr>
                    <th>名称</th>
                    <th>负责人</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.list.map(project => (
                        <tr key={project.id}>
                            <td>{project.name}</td>
                            <td>{props.users.find(user => user.id === project.personId)?.name}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    )
}