import React, {useState} from "react"
import ReactDOM from 'react-dom'

export default class index extends React.Component{
    state = {number: 0};
    handleClick= () => {
        // setTimeout(()=>{
        //     this.setState({number: this.state.number + 1}, () => {
        //         console.log('callback1', this.state.number)
        //     })
        //     console.log(this.state.number)
        //     this.setState({number: this.state.number + 1}, () => {
        //         console.log('callback2', this.state.number)
        //     })
        //     console.log(this.state.number)
        //     this.setState({number: this.state.number + 1}, () => {
        //         console.log('callback3', this.state.number)
        //     })
        //     console.log(this.state.number)
        // })
        
        setTimeout(()=>{
            this.setState({ number: 1  })
        })
        this.setState({ number: 2  })
        ReactDOM.flushSync(()=>{
            this.setState({ number: 3  })
        })
        this.setState({ number: 4  })
    }
    render() {
        return <div>
            {this.state.number}
            <button onClick={this.handleClick}>number++</button>
        </div>
    }
}