import React, {useState} from "react"

// 类
class textClass {
    sayHello = () => console.log('hello, my name is alien')
}

// 类组件
class Index extends React.Component {
    state = {message:`hello, world!`}
    sayHello = () => this.setState({message:'hello, my name is alien'})
    render() {
        return <div style={{marginTop: '50px'}} onClick={ this.sayHello }>{this.state.message}</div>
    }
}

// 函数
function textFun() {
    return 'hello, world';
}

// 函数组件
function FunComponent() {
    const [message, setMessage] = useState('hello,world')
    return <div onClick={() => setMessage('hello, my name is alien')}>{message}</div>
}