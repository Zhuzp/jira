import React, {useState} from "react"

class Person extends React.Component{
    constructor(props) {
        super(props);
        console.log('person')
    }
    componentDidMount() { console.log(1111) }
    eat(){}
    sleep(){}
    ddd(){}
    render() {
        return <div>person</div>
    }
}

class Programmer extends Person{
    constructor(props){
        super(props)
        console.log('programmer')
    }
    componentDidMount() {}
    code() {}
    render() {
        return <div>
            {super.render()}
            programmer
        </div>
    }
}