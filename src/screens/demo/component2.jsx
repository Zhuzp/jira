import React, {useState} from "react"


class Index extends React.Component {
    constructor(...arg) {
        super(...arg)
    }

    state = {}
    static number = 1
    handleClick = () => console.log(111)
    componentDidMount() {
        console.log(Index.number, Index.number1)
    }

    render() {
        return <div style={{marginTop: '50px'}} onClick={ this.handleClick }>hello, React</div>
    }
}

Index.number1 = 2;
Index.prototype.handleClick = () => console.log(222);

export default Index